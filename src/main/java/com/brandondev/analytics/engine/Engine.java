package com.brandondev.analytics.engine;

import com.brandondev.analytics.handler.AnalyticsUpdaterHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Engine {

    private AnalyticsUpdaterHandler analyticsUpdaterHandler;

    Engine(AnalyticsUpdaterHandler analyticsUpdaterHandler) {
        this.analyticsUpdaterHandler = analyticsUpdaterHandler;
    }

    @Scheduled(fixedDelay = 1000*60*60*4)
    public void runEngine() {
        try {
            analyticsUpdaterHandler.updateData();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
