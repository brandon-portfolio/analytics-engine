package com.brandondev.analytics.constant;

import java.util.HashMap;
import java.util.Map;

public class AnalyticsConstants {

    public static final String TYPE_PAGE_LOAD="PAGE_LOAD";
    public static final String TYPE_KEEP_ALIVE="KEEP_ALIVE";

    public static final int TYPE_PAGE_LOAD_TIME = 5; //5 seconds
    public static final int TYPE_KEEP_ALIVE_TIME = 30; //30 seconds

    public static final Map<String, Integer> TIME_MAP = new HashMap<>();

    static  {
        TIME_MAP.put(TYPE_PAGE_LOAD, TYPE_PAGE_LOAD_TIME);
        TIME_MAP.put(TYPE_KEEP_ALIVE, TYPE_KEEP_ALIVE_TIME);
    }
}
