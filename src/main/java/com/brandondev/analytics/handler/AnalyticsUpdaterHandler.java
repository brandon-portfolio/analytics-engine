package com.brandondev.analytics.handler;

import com.brandondev.analytics.entity.*;
import com.brandondev.analytics.repository.*;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AnalyticsUpdaterHandler {
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");

    private AnalyticsRepository analyticsRepository;
    private DailyReportRepository dailyReportRepository;
    private TotalsReportRepository totalsReportRepository;
    private UserRepository userRepository;
    private SessionRepository sessionRepository;

    public AnalyticsUpdaterHandler(AnalyticsRepository analyticsRepository, DailyReportRepository dailyReportRepository,
                                   TotalsReportRepository totalsReportRepository, UserRepository userRepository,
                                   SessionRepository sessionRepository) {
        this.analyticsRepository = analyticsRepository;
        this.dailyReportRepository = dailyReportRepository;
        this.totalsReportRepository = totalsReportRepository;
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }

    public void updateData() {
        List<AnalyticsEntity> entities = this.getNewAnalytics();
        TotalsReport totalsReport = this.getTotalsReport();
        Map<String, DailyReport> dailyReports = this.getDailyReports(entities);
        Map<String, User> userMap = getUsers(entities);
        Map<String, String> userIdMap = getUserIdMap(userMap);
        Map<String, Session> sessionMap = getSessions(entities);

        DataHandler dataHandler = new DataHandler(entities, totalsReport, dailyReports, userMap, userIdMap, sessionMap);
        dataHandler.run();

        totalsReportRepository.save(dataHandler.getTotalsReport());
        analyticsRepository.saveAll(dataHandler.getEntities());
        userRepository.saveAll(dataHandler.getUsers());
        dailyReportRepository.saveAll(dataHandler.getDailyReports());
        sessionRepository.saveAll(dataHandler.getSessions());
    }


    private Map<String, String> getUserIdMap(Map<String, User> userMap) {
        Map<String, String> userIdMap = new HashMap<>();
        userMap.values().forEach((User user)-> {
            user.getGuids().forEach((s)->{userIdMap.put(s, user.getId());});
            user.getIpAddresses().forEach((s)->{userIdMap.put(s, user.getId());});
        });
        return userIdMap;
    }

    private Map<String, User> getUsers(List<AnalyticsEntity> entities) {
        List<User> guidUsers = userRepository.findByGuidsIn(entities.stream().map(AnalyticsEntity::getUserId).collect(Collectors.toSet()));
        List<User> ipUsers = userRepository.findByIpAddressesIn(entities.stream().map(AnalyticsEntity::getIpAddress).collect(Collectors.toSet()));
        Map<String, User> users = new HashMap<>();
        guidUsers.forEach((User user)->{users.put(user.getId(), user);});
        ipUsers.forEach((User user)->{users.put(user.getId(), user);});
        return users;
    }

    private List<AnalyticsEntity> getNewAnalytics() {
        List<AnalyticsEntity> entities = analyticsRepository.findByLogged(false);
        List<AnalyticsEntity> goodEntities = new ArrayList<>();
        List<AnalyticsEntity> badEntities = new ArrayList<>();

        entities.forEach(entity -> {
            if(entity.getSessionId() == null) {
                entity.setFailed(true);
                badEntities.add(entity);
            } else
                goodEntities.add(entity);
        });

        analyticsRepository.saveAll(badEntities);
        return goodEntities;
    }

    private Map<String, DailyReport> getDailyReports(List<AnalyticsEntity> entities) {
        Set<String> keys = new HashSet<>();
        entities.forEach((entity) -> {
            keys.add(simpleDateFormat.format(entity.getDate()));
        });

        List<DailyReport> reports = dailyReportRepository.findByKeyIn(keys);
        Map<String, DailyReport> reportsMap = new HashMap<>();
        reports.forEach((report) -> {
            reportsMap.put(report.getKey(), report);
            keys.remove(report.getKey());
        });

        //create new daily reports if the database did not find any
        keys.forEach((key)->{
            DailyReport report = new DailyReport();
            try {
                report.setDate(simpleDateFormat.parse(key));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            report.setKey(key);
            report.setGuids(new HashSet<String>());
            report.setTotalPageLoads(0);

            reportsMap.put(key, report);
        });
        return reportsMap;
    }

    private TotalsReport getTotalsReport() {
        Optional<TotalsReport> reports = totalsReportRepository.findById("ONLY-ONE");
        if(reports.isPresent())
            return reports.get();
        TotalsReport report = new TotalsReport();
        report.setTotalPageLoads(0);
        report.setAverageVisitInSeconds(0);
        report.setAverageUserVisitInSeconds(0);

        report.setTotalSessions(0);
        report.setTotalUsers(0);

        report.setActionsTakenOnSession(new HashMap<>());
        report.setActionsTakenOnUser(new HashMap<>());
        report.setActionsTaken(new HashMap<>());

        report.setId("ONLY-ONE");
        return report;
    }


    private Map<String, Session> getSessions(List<AnalyticsEntity> entities) {
        List<Session> sessions = sessionRepository.findByIdIn(entities.stream().map(AnalyticsEntity::getSessionId).collect(Collectors.toSet()));
        Map<String, Session> sessionMap = new HashMap<>();
        sessions.forEach(session -> {sessionMap.put(session.getId(), session);});
        return sessionMap;
    }
}
