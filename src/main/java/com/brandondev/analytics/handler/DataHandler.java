package com.brandondev.analytics.handler;

import com.brandondev.analytics.constant.AnalyticsConstants;
import com.brandondev.analytics.entity.*;

import java.text.SimpleDateFormat;
import java.util.*;

public class DataHandler {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");

    private List<AnalyticsEntity> entities;
    private TotalsReport totalsReport;
    private Map<String, DailyReport> dailyReports;
    private Map<String, User> userMap;
    private Map<String, String> userIdMap;
    Map<String, Session> sessions;

    public DataHandler(List<AnalyticsEntity> entities, TotalsReport totalsReport,
                       Map<String, DailyReport> dailyReports, Map<String, User> userMap,
                       Map<String, String> userIdMap, Map<String, Session> sessions) {
        this.entities = entities;
        this.totalsReport = totalsReport;
        this.dailyReports = dailyReports;
        this.userMap = userMap;
        this.userIdMap = userIdMap;
        this.sessions = sessions;
    }

    public void run() {
        System.out.println("ENTITIES SIZE " + entities.size());

        entities.forEach((entity -> {
            if(entity.getUserId() != null || entity.getIpAddress() != null) {
                String guid = this.processUser(entity);
                processSession(entity, guid);
                processDailyReport(entity, guid);
                processTotals(entity);
                entity.setLogged(true);
                return;
            }
        }));
    }

    private void processTotals(AnalyticsEntity entity) {
        totalsReport.addAction(entity.getAction());
        if(entity.getAction().equals(AnalyticsConstants.TYPE_PAGE_LOAD))
            totalsReport.addPageLoad();
    }

    private void processDailyReport(AnalyticsEntity analytics, String guid) {
        String key = simpleDateFormat.format(analytics.getDate());
        boolean isPageLoad = analytics.getAction().equals(AnalyticsConstants.TYPE_PAGE_LOAD);
        dailyReports.get(key).getGuids().add(guid);
        if(isPageLoad)
            dailyReports.get(key).addTotalPageLoads();
    }

    private void processSession(AnalyticsEntity entity, String guid) {
        String sessionId = entity.getSessionId();
        boolean newSession = !sessions.containsKey(sessionId);

        if(newSession) {
            createSession(sessionId, guid);
        }

        int startingTimeValue = sessions.get(sessionId).getTotalTimeOnSiteInSeconds();

        String action = entity.getAction();
        Integer timeAdded = AnalyticsConstants.TIME_MAP.get(action);
        if(timeAdded != null)
            sessions.get(sessionId).addTime(timeAdded);

        boolean newAction = sessions.get(sessionId).addAction(action);

        if(newSession)
            totalsReport.addSession();

        if(timeAdded != null)
            totalsReport.addSessionTime(newSession, startingTimeValue, sessions.get(sessionId).getTotalTimeOnSiteInSeconds());

        if(newAction)
            totalsReport.addNewSessionAction(action);
    }

    private String processUser(AnalyticsEntity entity) {
        String guid = userIdMap.containsKey(entity.getUserId()) ? userIdMap.get(entity.getUserId()) :
                userIdMap.containsKey(entity.getIpAddress()) ? userIdMap.get(entity.getIpAddress()) :
                null;

        boolean newUser = guid==null;
        if(newUser)
            guid=createUser(entity.getUserId(), entity.getIpAddress());

        if(entity.getUserId() != null)
            userMap.get(guid).addGuid(entity.getUserId());
        if(entity.getIpAddress() != null)
            userMap.get(guid).addIpAddress(entity.getIpAddress());

        int startTime = userMap.get(guid).getTotalTimeOnSiteInSeconds();

        String action = entity.getAction();
        Integer timeAdded = AnalyticsConstants.TIME_MAP.get(action);
        if(timeAdded != null)
            userMap.get(guid).addTime(timeAdded);

        boolean newAction = userMap.get(guid).addAction(action);


        if(newUser)
            totalsReport.addUser();

        if(timeAdded != null)
            totalsReport.addSessionTime(newUser, startTime, userMap.get(guid).getTotalTimeOnSiteInSeconds());

        if(newAction)
            totalsReport.addNewUserAction(action);

        return guid;
    }



    private void createSession(String sessionId, String userId) {
        Session session = new Session();
        session.setId(sessionId);
        session.setActionsTaken(new HashMap<>());
        session.setTotalTimeOnSiteInSeconds(0);
        session.setUserId(userId);
        sessions.put(sessionId, session);
    }

    private String createUser(String userId, String ipAddress) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setActionsTaken(new HashMap<>());
        user.setTotalTimeOnSiteInSeconds(0);
        user.setGuids(new HashSet<>());
        user.setIpAddresses(new HashSet<>());
        if(userId != null) {
            userIdMap.put(userId, user.getId());
            user.addGuid(userId);
        }

        if(ipAddress != null) {
            userIdMap.put(ipAddress, user.getId());
            user.addIpAddress(ipAddress);
        }

        userMap.put(user.getId(), user);
        return user.getId();
    }



    public TotalsReport getTotalsReport() {
        return totalsReport;
    }

    public List<AnalyticsEntity> getEntities() {
        return entities;
    }

    public List<User> getUsers() {
        return new ArrayList<>(userMap.values());
    }

    public List<DailyReport> getDailyReports() {
        return new ArrayList<>(dailyReports.values());
    }

    public List<Session> getSessions() {
        return new ArrayList<>(sessions.values());
    }



}
