package com.brandondev.analytics.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "TotalsReport")
@Data
public class TotalsReport {
    @Id
    private String id;

    private int totalPageLoads;

    private double averageVisitInSeconds;
    private double averageUserVisitInSeconds;

    private int totalSessions;
    private int totalUsers;

    private Map<String, Integer> actionsTaken;
    private Map<String, Integer> actionsTakenOnSession;
    private Map<String, Integer> actionsTakenOnUser;

    public void addSession() {
        totalSessions++;
    }

    public void addUser() {
        totalUsers++;
    }

    public void addPageLoad() {
        totalPageLoads++;
    }

    public void addSessionTime(boolean newSession, int startTime, int newTime) {
        if(!newSession) {
            averageVisitInSeconds-=(double)startTime/(double)totalSessions;
        }
        averageVisitInSeconds+=(double)newTime/(double)totalSessions;
    }

    public void addNewSessionAction(String action) {
        if(!actionsTakenOnSession.containsKey(action))
            actionsTakenOnSession.put(action,0);
        actionsTakenOnSession.put(action, actionsTakenOnSession.get(action)+1);
    }

    public void addUserTime(boolean newSession, int startTime, int newTime) {
        if(!newSession) {
            averageUserVisitInSeconds-=(double)startTime/(double)totalUsers;
        }
        averageUserVisitInSeconds+=(double)newTime/(double)totalUsers;
    }

    public void addNewUserAction(String action) {
        if(!actionsTakenOnUser.containsKey(action))
            actionsTakenOnUser.put(action,0);
        actionsTakenOnUser.put(action, actionsTakenOnUser.get(action)+1);
    }

    public void addAction(String action) {
        if(!actionsTaken.containsKey(action))
            actionsTaken.put(action, 0);
        actionsTaken.put(action, actionsTaken.get(action)+1);
    }
}
