package com.brandondev.analytics.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@Document(collection="Sessions")
public class Session {
    @Id
    private String id; //the sessionId assigned by UI
    private String userId;

    private Map<String, Integer> actionsTaken;
    private int totalTimeOnSiteInSeconds;


    public void addTime(int time) {
        totalTimeOnSiteInSeconds+=time;
    }

    public boolean addAction(String action) {
        if(!actionsTaken.containsKey(action)) {
            actionsTaken.put(action, 1);
            return true;
        } else {
            actionsTaken.put(action, actionsTaken.get(action)+1);
            return false;
        }
    }

}
