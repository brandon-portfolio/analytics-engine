package com.brandondev.analytics.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.Set;

@Document(collection="Users")
@Data
public class User {
    @Id
    private String id;

    //used for determining a user across IP addresses and assigned guids
    private Set<String> guids;
    private Set<String> ipAddresses;

    private Map<String, Integer> actionsTaken;
    private int totalTimeOnSiteInSeconds;

    public void addGuid(String guid) {
        guids.add(guid);
    }

    public void addIpAddress(String ipAddress) {
        ipAddresses.add(ipAddress);
    }

    public void addTime(int time) {
        totalTimeOnSiteInSeconds+=time;
    }

    public boolean addAction(String action) {
        if(!actionsTaken.containsKey(action)) {
            actionsTaken.put(action, 1);
            return true;
        } else {
            actionsTaken.put(action, actionsTaken.get(action)+1);
            return false;
        }
    }

}
