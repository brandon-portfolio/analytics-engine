package com.brandondev.analytics.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection="Analytics")
public class AnalyticsEntity {

    @Id
    private String id;

    private Date date;
    private boolean logged;
    private boolean failed; //missing sessionId

    private String sessionId;
    private String userId;
    private String ipAddress;
    private String userAgent;

    private String action; //PAGE LOAD, BUTTON CLICK, ETC...

    private int screenWidth;
    private int screenHeight;

    private int viewportWidth;
    private int viewportHeight;
}
