package com.brandondev.analytics.repository;

import com.brandondev.analytics.entity.TotalsReport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TotalsReportRepository extends MongoRepository<TotalsReport, String> {
}
