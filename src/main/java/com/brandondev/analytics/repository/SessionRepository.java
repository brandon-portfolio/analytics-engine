package com.brandondev.analytics.repository;

import com.brandondev.analytics.entity.Session;
import com.brandondev.analytics.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

public interface SessionRepository extends MongoRepository<Session, String> {

    public List<Session> findByIdIn(Set<String> id);


}
