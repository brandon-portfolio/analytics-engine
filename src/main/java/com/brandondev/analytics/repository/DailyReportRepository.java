package com.brandondev.analytics.repository;

import com.brandondev.analytics.entity.DailyReport;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

public interface DailyReportRepository extends MongoRepository<DailyReport, String> {

    public List<DailyReport> findByKeyIn(Set<String> key);
}
