package com.brandondev.analytics.repository;

import com.brandondev.analytics.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

public interface UserRepository extends MongoRepository<User, String> {

    public List<User> findByGuidsIn(Set<String> guids);
    public List<User> findByIpAddressesIn(Set<String> ipAddresses);

}
