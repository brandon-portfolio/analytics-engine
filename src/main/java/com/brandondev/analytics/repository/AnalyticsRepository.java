package com.brandondev.analytics.repository;

import com.brandondev.analytics.entity.AnalyticsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnalyticsRepository extends MongoRepository<AnalyticsEntity, String> {

    public List<AnalyticsEntity> findByLogged(boolean logged);

}
